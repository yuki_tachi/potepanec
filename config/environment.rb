# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

Money.locale_backend = :i18n
