require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon, name: "Sapphire on Rails") }
    let!(:product) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:products_in_taxon1) { create_list(:product, 3, taxons: [taxon1]) }
    let!(:products_in_taxon2) { create_list(:product, 3, taxons: [taxon2]) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
      expect(response).to render_template("products/show")
    end

    it "has set correct product values" do
      expect(assigns(:product)).to eq product
    end

    it "set correct related_products value" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
