require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  before do
    get :index
  end

  it "responds successfully" do
    expect(response).to be_successful
    expect(response).to render_template("home/index")
  end
end
