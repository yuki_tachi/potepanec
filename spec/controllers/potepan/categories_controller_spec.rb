require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:other_product) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "responds successfully" do
      expect(response).to be_successful
      expect(response).to render_template("categories/show")
    end

    it "has set correct values" do
      puts(taxon.products.pluck(:name))
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to include product
      expect(assigns(:products)).not_to include other_product
    end
  end
end
