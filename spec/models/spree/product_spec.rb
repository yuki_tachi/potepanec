require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon1) { create(:taxon) }
  let(:taxon2) { create(:taxon, name: "Sapphire on Rails") }
  let(:taxon3) { create(:taxon, name: "Emerald on Rails") }
  let!(:product) { create(:product, taxons: [taxon1, taxon2]) }
  let!(:products_in_taxon1) { create_list(:product, 3, taxons: [taxon1]) }
  let!(:products_in_taxon2) { create_list(:product, 3, taxons: [taxon2]) }
  let!(:product_in_taxon3) { create(:product, taxons: [taxon3]) }

  it "set correct related_products value" do
    expect(product.related_products).not_to include product_in_taxon3
  end
end
