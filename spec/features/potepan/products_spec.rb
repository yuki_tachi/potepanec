require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon1) { create(:taxon) }
  let(:taxon2) { create(:taxon, name: "Sapphire on Rails") }
  let(:taxon3) { create(:taxon, name: "Emerald on Rails") }
  let!(:product) { create(:product, name: "Rails potepan product", taxons: [taxon1, taxon2]) }
  let!(:product_in_taxon1) { create(:product, taxons: [taxon1]) }
  let!(:product_in_taxon2) { create(:product, taxons: [taxon2]) }
  let!(:product_in_taxon3) { create(:product, taxons: [taxon3]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "products show page" do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_content product.name
    expect(page).to have_content product.price
    expect(page).to have_content '$'
    expect(page).to have_content product.description
  end

  scenario "redirects to potepan_category_path when '一覧ページへ戻る' is clicked" do
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon1.id)
  end

  scenario "redirects to potepan_root_path when 'Home' is clicked in light section " do
    within(".breadcrumb") do
      click_link "Home"
      expect(current_path).to eq potepan_root_path
    end
  end

  scenario "redirects to potepan_root_path when 'Home' is clicked in header section " do
    within(".navbar-right") do
      click_link "Home"
      expect(current_path).to eq potepan_root_path
    end
  end

  scenario "show related product page" do
    expect(page).to have_content product_in_taxon1.name
    expect(page).to have_content product_in_taxon2.name
    expect(page).not_to have_content product_in_taxon3.name
  end

  scenario "redirects to related product page" do
    click_link product_in_taxon1.name
    expect(current_path).to eq potepan_product_path(product_in_taxon1.id)
  end

 # scenario "products search page", focus: true do
 #   visit potepan_root_path
 #   fill_in "searchWord", with: "Rails"
 #   click_on "検索"
 #   expect(current_path).to eq search_potepan_path
 #   expect(page).to have_content product.name
 # end

end
