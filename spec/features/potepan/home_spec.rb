require 'rails_helper'

RSpec.feature "Potepan::Homes", type: :feature do
  let!(:product) { create_list(:product, 2, available_on: Date.current.in_time_zone, taxons: [taxon1]) }
  let!(:product_yesterday) { create_list(:product, 2, available_on: 1.day.ago, taxons: [taxon1]) }
  let!(:product_2daysago) { create_list(:product, 2, available_on: 2.day.ago, taxons: [taxon1]) }
  let!(:product_3daysago) { create_list(:product, 2, available_on: 3.day.ago, taxons: [taxon1]) }
  let(:taxon1) { create(:taxon) }

  # 新着商品が表示されていること
  scenario "shows new products" do
    aggregate_failures do
      visit potepan_root_path
      expect(page).to have_content product[0].name
      expect(page).to have_content product_2daysago[1].name
    end
  end

  scenario "redirects to potepan_product_path when new product is clicked" do
    aggregate_failures do
      visit potepan_root_path
      within(".featuredProducts") do
        click_link product[0].name
        expect(current_path).to eq potepan_product_path(product[0].id)
      end
    end
  end
end
