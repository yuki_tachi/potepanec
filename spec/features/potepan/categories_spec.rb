require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: parent_taxon.id) }
  let!(:taxonomy) { create(:taxonomy) }
  let(:parent_taxon) { create(:taxon, taxonomy: taxonomy, name: "parent_ruby", parent: taxonomy.root) }
  let!(:product) { create(:product, name: "Rspec_product", taxons: [taxon], option_types: [option_type_color, option_type_size]) }
  let(:option_type_color) { create(:option_type, presentation: "Color", option_values: [option_value_color, option_value_color_other]) }
  let(:option_value_color) { create(:option_value, name: "Red") }
  let(:option_type_size) { create(:option_type, presentation: "Size", option_values: [option_value_size, option_value_size_other]) }
  let(:option_value_size) { create(:option_value, name: "Small") }

  let!(:product_cheaper) { create(:product, taxons: [taxon], option_types: [option_type_color, option_type_size], price: 0.01) }
  let!(:product_newer) { create(:product, taxons: [taxon], option_types: [option_type_color, option_type_size], available_on: 1.day.ago) }

  let!(:product_other_taxon) { create(:product) }
  let!(:product_other_color) { create(:product, taxons: [taxon], option_types: [option_type_size]) }
  let(:option_value_color_other) { create(:option_value, name: "Blue") }
  let!(:product_other_size) { create(:product, taxons: [taxon], option_types: [option_type_color]) }
  let(:option_value_size_other) { create(:option_value, name: "Large") }

  let(:product_show_block) { page.all(".media-body")}

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario "categories show page" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).to have_content taxonomy.name
    expect(page).to have_content parent_taxon.name
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_content product_other_color.name
    expect(page).not_to have_content product_other_taxon.name
  end

  scenario "redirects to product_path" do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario "shows sidebar to apply filter by color" do
    aggregate_failures do
      expect(page).to have_content("Red(4)")
      click_link "Red"
      expect(page).to have_content product.name
      expect(page).not_to have_content product_other_color.name
    end
  end

  scenario "shows sidebar to apply filter by size" do
    aggregate_failures do
      expect(page).to have_content("Small(4)")
      click_link "Small"
      expect(page).to have_content product.name
      expect(page).not_to have_content product_other_size.name
    end
  end

  scenario "sort by newess by default" do
    expect(product_show_block.first).to have_content product_newer.name
  end

  #scenario "sort by oldness by default", js:true do
    #select '古い順', from: 'sort'
    #page.evaluate_script("$('sort').trigger('change')")
    #puts(current_url)
    #expect(product_show_block.last).to have_content product_newer.name
  #end
end
