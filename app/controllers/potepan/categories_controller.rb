class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxon_roots = Spree::Taxon.where(depth: 0).includes(:children)
    @colors = Spree::OptionValue.option_values_all("Color")
    @sizes = Spree::OptionValue.option_values_all("Size")

    if params[:color].present? && params[:size].present?
      @products = Spree::Product.filter_by_taxon(@taxon).filter_by_option_2value(params[:color], params[:size])
    elsif params[:color].present? || params[:size].present?
      option_value = params[:color].present? ? params[:color] : params[:size]
      @products = Spree::Product.filter_by_taxon(@taxon).filter_by_option_value(option_value)
    else
      @products = Spree::Product.filter_by_taxon(@taxon)
    end

    if params[:sort].present?
      case params[:sort]
      when "sort_by_newness"
        @products = @products.sort_by_newness
      when "sort_by_oldness"
        @products = @products.sort_by_oldness
      when "sort_by_price_desc"
        @products = @products.sort_by_price_desc
      when "sort_by_price_asc"
        @products = @products.sort_by_price_asc
      end
    else
      @products = @products.sort_by_newness
    end


    @partial = params[:view] || 'list'
    respond_to do |format|
      format.html
      format.js
    end
  end
end
