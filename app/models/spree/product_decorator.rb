Spree::Product.class_eval do
  def related_products
    Spree::Product.in_taxons(taxons).includes(master: [:default_price, :images]).distinct.where.not(id: id)
  end

  def self.count_products_with_option_value(taxon, option_value)
    Spree::Product.in_taxon(taxon).includes(option_types: :option_values).where(spree_option_values: { name: option_value }).count
  end

  def self.ransackable_attributes(auth_object = nil)
    %w(name description)
  end

  def ransackable_associations(auth_object = nil)
    %w(stores variants_including_master master variants)
  end

  scope :filter_by_taxon, -> (taxon) { in_taxon(taxon).includes(master: [:default_price, :images]).includes(option_types: :option_values).distinct }

  scope :filter_by_option_value, -> (value) do
    includes(master: [:default_price, :images]).includes(option_types: :option_values).where(spree_option_values: { name: value })
  end

  scope :filter_by_option_2value, -> (value1, value2) do
    includes(master: [:default_price, :images]).includes(option_types: :option_values).where(spree_option_values: { name: value1, name: value2 })
  end

  scope :sort_by_newness, -> {reorder(available_on: :desc)}
  scope :sort_by_oldness, -> {reorder(available_on: :asc)}
  scope :sort_by_price_desc, -> {unscope(:order).descend_by_master_price}
  scope :sort_by_price_asc, -> {unscope(:order).ascend_by_master_price}
end
