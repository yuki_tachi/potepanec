module ApplicationHelper
  def full_title(page_title)
    base_title = 'BIGBAG Store'
    if page_title.blank?
      return base_title
    else
      return "#{page_title} - #{base_title}"
    end
  end
end
